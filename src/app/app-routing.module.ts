import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HitoricoPorMesComponent } from "./fatura/historico-por-mes/hitorico-por-mes.component";

const routes: Routes = [{ path: "", component: HitoricoPorMesComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
