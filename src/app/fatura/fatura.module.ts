import { NgModule } from "@angular/core";

import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { HitoricoPorMesComponent } from "./historico-por-mes/hitorico-por-mes.component";
import { FaturaService } from "./services/fatura.service";
import { FaturaRoutingModule } from "./fatura-routing.module";
import { FormsModule } from "@angular/forms";

import {
  MatTableModule,
  MatListModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
} from "@angular/material/";

@NgModule({
  declarations: [HitoricoPorMesComponent],
  imports: [
    CommonModule,
    RouterModule,
    FaturaRoutingModule,
    FormsModule,
    MatTableModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [FaturaService],
  bootstrap: [],
})
export class FaturaModule {}
