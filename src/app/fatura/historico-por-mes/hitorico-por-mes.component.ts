import { Component, OnInit } from "@angular/core";
import { FaturaService } from "../services/fatura.service";
import { element } from "protractor";

@Component({
  selector: "app-hitorico-por-mes",
  templateUrl: "./hitorico-por-mes.component.html",
  styleUrls: ["./hitorico-por-mes.component.css"],
})
export class HitoricoPorMesComponent implements OnInit {
  public lancamentos = new Array<any>();
  public lancamentoDetalhado = new Array<any>();
  public categorias = new Array<any>();
  public consolidado = new Array<any>();
  public mesDetalhado: string;
  public displayedColumns: string[] = ["origem", "categoria", "valor"];
  public showTable: boolean;

  constructor(private faturaService: FaturaService) {}

  ngOnInit() {
    this.getAsyncData();
  }

  async getAsyncData() {
    this.lancamentos = await this.faturaService.getInvoce();
    this.categorias = await this.faturaService.getCategorias();

    this.geraConsolidado();
  }

  public geraConsolidado() {
    for (let i = 1; i < 12; i++) {
      const filtraLancamentos = (lancamentos) =>
        lancamentos.mes_lancamento === i;
      const lancamentosFiltrados = this.lancamentos.filter(filtraLancamentos);

      var resultado = lancamentosFiltrados.reduce(function (soma, valorFinal) {
        return soma + valorFinal.valor;
      }, 0);
      if (resultado > 0) {
        this.consolidado.push({
          idMes: i,
          mes: this.retornaMes(i),
          valor: resultado,
        });
      }
    }
  }
  public geraDetalhamento(mes: number): any {
    this.showTable = true;

    this.lancamentoDetalhado = this.lancamentos.filter(function (obj) {
      return obj.mes_lancamento == mes;
    });

    this.lancamentoDetalhado.forEach((element) => {
      element.categoria = this.retornaCategoria(element.categoria - 1);
    });
  }

  public retornaCategoria(categoria): string {
    return this.categorias[categoria].nome;
  }

  public retornaMes(mes): string {
    const mesesDoAno = [
      { id: 0, nome: "" },
      { id: 1, nome: "Janeiro" },
      { id: 2, nome: "Fevereiro" },
      { id: 3, nome: "Março" },
      { id: 4, nome: "Abril" },
      { id: 5, nome: "Maio" },
      { id: 6, nome: "Junho" },
      { id: 7, nome: "Julho" },
      { id: 8, nome: "Agosto" },
      { id: 9, nome: "Setembro" },
      { id: 10, nome: "Outubro" },
      { id: 11, nome: "Novembro" },
      { id: 12, nome: "Dezembro" },
    ];
    return mesesDoAno[mes].nome;
  }
}
