import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HitoricoPorMesComponent } from './hitorico-por-mes.component';

describe('HitoricoPorMesComponent', () => {
  let component: HitoricoPorMesComponent;
  let fixture: ComponentFixture<HitoricoPorMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HitoricoPorMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HitoricoPorMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
