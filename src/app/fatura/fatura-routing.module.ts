import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HitoricoPorMesComponent } from "./historico-por-mes/hitorico-por-mes.component";

const faturaRoutes: Routes = [
  {
    path: "",
    component: HitoricoPorMesComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(faturaRoutes)],
  exports: [RouterModule],
  providers: [],
  bootstrap: [],
})
export class FaturaRoutingModule {}
