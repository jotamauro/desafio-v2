import { TestBed } from "@angular/core/testing";

import { FaturaService } from "./fatura.service";

describe("FaturaServiceService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: FaturaService = TestBed.get(FaturaService);
    expect(service).toBeTruthy();
  });
});
